@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">
                                <h2>Edit answer for Question: {{ $question->title }} </h2>
                            </div>
                            <hr>
                            <form action="{{ route('questions.answers.update', [$question->id, $answer->id]) }}"
                                  method="POST">
                                @csrf
                                @method('PATCH')
                                <div class="form-group">
                                    <textarea name="body" cols="30" rows="7"
                                              class="form-control {{ $errors->has('body', $answer->body) ? 'is-invalid' : '' }}">{{ old('body',$answer->body) }}</textarea>
                                    @if($errors->has('body'))
                                        <div class="invalid-feedback">{{ $errors->first('body') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-primary">Submit your answer</button>
                                </div>
                            </form>
                            <a href="{{ route('questions.show', $question->slug) }}" class="btn btn-block btn-dark">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
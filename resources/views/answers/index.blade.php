<div class="row mt-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h2>{{ $answersCount . " " . str_plural('Answer', $answersCount)  }}</h2>
                </div>
                <hr>
                {{-- Display messages --}}
                @include('partials._messages')

                @foreach($answers as $answer)
                    <div class="media">
                        <div class="d-flex flex-column votes-control">
                            <a href="#" class="vote-up" title="This answer is useful">
                                <i class="fa fa-caret-up fa-4x" aria-hidden="true"></i>
                            </a>
                            <span class="votes-count">12</span>
                            <a href="#" class="vote-down off" title="This answer is not useful">
                                <i class="fa fa-caret-down fa-4x" aria-hidden="true"></i>
                            </a>
                            <a href="#" title="This is the best answer" class="mt-21 {{ $answer->status }}">
                                <i class="fa fa-check fa-2x" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            {!! $answer->body_html !!}
                            <div class="row">
                                <div class="col-4">
                                    <div class="ml-auto">
                                        @can('update', $answer)
                                            <a href="{{ route('questions.answers.edit', [$question->id, $answer->id]) }}" class="btn btn-outline-success btn-sm">Edit</a>
                                        @endcan
                                        {{--Using gates delete-question--}}
                                        {{--@can('delete-question', $question)--}}
                                        {{--Using policy delete--}}
                                        @can('delete', $answer)
                                            <form action="{{ route('questions.answers.destroy', [$question->id, $answer->id]) }}" class="delete-form" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                            </form>
                                        @endcan
                                        {{--@endcan--}}
                                    </div>
                                </div>
                                <div class="col-4"></div>
                                <div class="col-4">
                                    <span class="text-muted">Answered: {{ $answer->creation_date }}</span>
                                    <div class="media">
                                        <a href="{{ $answer->user->url }}" class="pr-2">{{ $answer->user->name }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
</div>
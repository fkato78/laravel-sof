@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h3 style="margin-bottom: 0">All Questions</h3>
                            <div class="ml-auto">
                                <a href="{{ route('questions.create') }}" class="btn btn-primary">Ask Question</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {{-- Display messages --}}
                        @include('partials._messages')
                        @foreach($questions as $question)
                            <div class="media mb-4">
                                <div class="d-flex flex-column counters mr-5">
                                    <div class="vote">
                                        <strong>{{ $question->votes }}</strong> {{ str_plural('vote', $question->votes) }}
                                    </div>
                                    <div class="status mb-3 {{ $question->status }}">
                                        <strong>{{ $question->answers_count }}</strong> {{ str_plural('answer', $question->answers_count) }}
                                    </div>
                                    <div class="view">
                                        {{ $question->views . " " . str_plural('view', $question->views) }}
                                    </div>
                                </div>
                                <div class="media-body">
                                    <div class="d-flex align-items-center">
                                        <h3 style="margin-bottom: 0"><a href="{{ $question->url }}">{{ $question->title }}</a></h3>
                                        <div class="ml-auto">
                                            @can('update-question', $question)
                                                <a href="{{ route('questions.edit', $question->id) }}" class="btn btn-outline-success btn-sm">Edit</a>
                                            @endcan
                                                {{--Using gates delete-question--}}
                                            {{--@can('delete-question', $question)--}}
                                                {{--Using policy delete--}}
                                            @can('delete', $question)
                                                <form action="{{ route('questions.destroy', $question->id) }}" class="delete-form" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                                </form>
                                            @endcan
                                            {{--@endcan--}}
                                        </div>
                                    </div>
                                    <p class="lead">
                                        Asked by: <a href="{{ $question->user->url }}">{{ $question->user->name }}</a>
                                        <small class="text-muted">{{ $question->creation_date }}</small>
                                    </p>

                                    {{ str_limit($question->body, 250) }}
                                </div>
                            </div>
                            <hr>
                        @endforeach
                        {{--  Pagination links --}}
                        {{ $questions->links('vendor.pagination.bootstrap-4') }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

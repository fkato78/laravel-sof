@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <div class="d-flex align-items-center">
                                <h2 style="margin-bottom: 0">{{ $question->title }}</h2>
                                <div class="ml-auto">
                                    <a href="{{ route('questions.index') }}" class="btn btn-secondary">All Questions</a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="media">
                            <div class="d-flex flex-column votes-control">
                                <a href="#" class="vote-up" title="This question is useful">
                                    <i class="fa fa-caret-up fa-4x" aria-hidden="true"></i>
                                </a>
                                <span class="votes-count">12</span>
                                <a href="#" class="vote-down off" title="This question is not useful">
                                    <i class="fa fa-caret-down fa-4x" aria-hidden="true"></i>
                                </a>
                                <a href="#" title="Favorite question" class="favorite mt-21 favorited">
                                    <i class="fa fa-star fa-2x" aria-hidden="true"></i>
                                    <span class="favorites-count">234</span>
                                </a>
                            </div>
                            <div class="media-body">
                                {!! $question->body_html !!}
                                <div class="float-right">
                                    <span class="text-muted">Asked: {{ $question->creation_date }}</span>
                                    <div class="media">
                                        <a href="{{ $question->user->url }}" class="pr-2">{{ $question->user->name }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- The answers part(All answers from index) --}}
        @include('answers.index', [
            'answers' => $question->answers,
            'answersCount' => $question->answers_count
        ])

        {{-- Create the answer part(from create) --}}
        @include('answers.create')
    </div>
@endsection

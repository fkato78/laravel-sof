<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /** fillable fields(Mass Assignment)  */
    protected $fillable = ['title', 'slug', 'body', 'views', 'votes', 'answers', 'user_id', 'best_answer_id'];

    /** Association with User Model */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /** Generate slug from title via mutator
     *  So when title is created the slug will create .
     * @param $value
     */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }
    
    /** Accessor url */
    public function getUrlAttribute()
    {
        return route('questions.show', $this->slug);
    }

    /** Accessor creation_date */
    public function getCreationDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    /** Accessor status: add css classes according the question answer status */
    public function getStatusAttribute()
    {
        if($this->answers_count) {
            // if the question answer accepted
            if($this->best_answer_id){
                return "answer-accepted";
            }
            return "answered";
        }
        return "unanswered";
    }
    
    /** Accessor body_html */
    public function getBodyHtmlAttribute()
    {
        return \Parsedown::instance()->text($this->body);
    }

    /** Association with Answer Model */
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }


}

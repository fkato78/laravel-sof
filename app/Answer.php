<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['user_id', 'question_id', 'votes_count', 'body'];

    /** Association with User Model */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /** Association with Question Model */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    /** Accessor body_html */
    public function getBodyHtmlAttribute()
    {
        return \Parsedown::instance()->text($this->body);
    }

    /** Increment answers_count when answer is created via created event */
    public static function boot()
    {
        parent::boot();

        // Increment when created
        static::created(function($answer) {
            $answer->question->increment('answers_count');
        });

        // Decrement when deleted
        static::deleted(function($answer) {
            $answer->question->decrement('answers_count');
        });

    }

    /** Accessor creation date */
    public function getCreationDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    /** Accessor answer status */
    public function getStatusAttribute()
    {
        return $this->id = $this->question->best_answer_id ? 'vote-accepted' : '';
    }
}

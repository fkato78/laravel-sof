<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use App\Question;
use Illuminate\Support\Facades\Gate;

class QuestionsController extends Controller
{
    /** @var int Pagination limit */
    const PAGINATION_LIMIT = 5;

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = "All Questions";
        $questions = Question::with('user')->latest()->paginate(self::PAGINATION_LIMIT);
        return view('questions.index', ['questions' => $questions, 'pageTitle' => $pageTitle]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $question = new Question();
        return view('questions.create', compact('question') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param QuestionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(QuestionRequest $request)
    {
        // Since User has many Questions
        // create question
        $request->user()->questions()->create($request->only('title', 'body'));

        // Redirect
        return redirect()->route('questions.index')->with('success', 'Question created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        // Increment thr question views
        $question->increment('views');

        return view('questions.show', compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        // Authorization using Gates
        if(Gate::denies('update-question', $question)){
            abort(403,'Access denied');
        }
        return view('questions.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     * @param QuestionRequest $request
     * @param Question $question
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(QuestionRequest $request, Question $question)
    {

        /** Authorization via Gates */
        if(Gate::denies('update-question', $question)){
            abort(403,'Access denied');
        }
        $question->update($request->only('title', 'body'));

        // Redirection after update
        return redirect()->route('questions.index')->with('success', 'Question updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question $question
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Question $question)
    {
        /*
        if(Gate::denies('delete-question', $question)){
            abort(403,'Access denied');
        }
        */
        /** Authorization via Policy */
        $this->authorize('delete', $question);

        if($question->delete()){
            return redirect()->route('questions.index')->with('success', 'Question deleted');
        }
    }
}

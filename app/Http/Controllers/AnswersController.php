<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Question $question
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Question $question,Request $request)
    {
        // Validation
        $request->validate([
            'body' => 'required'
        ]);
        // Save in DB(since Question hasMany Answers)
        $question->answers()->create([
            'body' => $request->body,
            'user_id' => auth()->id()
        ]);

        // Redirect back after submit answer
        return back()->with('success', 'Answer submitted');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Question $question
     * @param  \App\Answer $answer
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Question $question, Answer $answer)
    {
        // Only authorized user
        $this->authorize('update', $answer);

        return view('answers.edit', compact('answer', 'question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Question $question
     * @param  \App\Answer $answer
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Question $question, Answer $answer)
    {
        // Only authorized user: update is the name of the policy action
        $this->authorize('update', $answer);

        // Validation and update
        $answer->update($request->validate([
            'body' => 'required|min:3'
        ]));

        // Redirection: Remember that the show route need slug
        return redirect()->route('questions.show', $question->slug)->with('success', 'Answer has been updated successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question,Answer $answer)
    {
        // Only authorized user: delete is the name of policy action
        $this->authorize('delete', $answer);

        // Delete the answer
        $answer->delete();

        // Redirect back
        return back()->with('success', 'Answer deleted successfully!');
    }
}

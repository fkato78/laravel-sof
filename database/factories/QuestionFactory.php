<?php

use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        // Remove the . from the end of the title
        // Since title generated via faker it will ends with .
        'title' => rtrim($faker->sentence(rand(5, 10))),
        'body' => $faker->paragraphs(rand(3, 8), true),
        'views' => rand(0, 10),
//        'answers_count' => rand(0, 10),
        'votes' => rand(-4, 10),
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(App\Answer::class, function (Faker $faker) {
    return [
        'body' => $faker->paragraphs(rand(2, 6), true),
        'votes_count' => rand(2, 7),
        'user_id' => App\User::pluck('id')->random()
    ];
});
